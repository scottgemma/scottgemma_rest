package com.citi.training.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebCustomersApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebCustomersApplication.class, args);
	}

}
